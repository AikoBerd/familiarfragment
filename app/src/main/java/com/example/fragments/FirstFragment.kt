package com.example.fragments

import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import kotlin.random.Random

class FirstFragment : Fragment() {

    private var fragment2BackgroundColor: Int = Color.WHITE
    private var fragment3BackgroundColor: Int = Color.WHITE
    private var isSwapped = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_fisrt, container, false)

        view.findViewById<Button>(R.id.changeColor).setOnClickListener {
            fragment2BackgroundColor = Color.rgb(Random.nextInt(256), Random.nextInt(256), Random.nextInt(256))
            fragment3BackgroundColor = Color.rgb(Random.nextInt(256), Random.nextInt(256), Random.nextInt(256))
            updateFragmentBackgroundColors()
        }

        view.findViewById<Button>(R.id.swap).setOnClickListener {
            isSwapped = !isSwapped
            swapFragments()
        }

        updateFragmentBackgroundColors()

        return view
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        restoreFragmentState()
    }

    override fun onStop() {
        super.onStop()
        saveFragmentState()
    }

    private fun saveFragmentState(){
        arguments?.putBoolean(IS_SWAPPED_KEY, isSwapped)
        arguments?.putInt(BACKGROUND_COLOR2_KEY, fragment2BackgroundColor)
        arguments?.putInt(BACKGROUND_COLOR3_KEY, fragment3BackgroundColor)
    }

    private fun restoreFragmentState() {
        arguments?.let {
            isSwapped = it.getBoolean(IS_SWAPPED_KEY)
            fragment2BackgroundColor = it.getInt(BACKGROUND_COLOR2_KEY)
            fragment3BackgroundColor = it.getInt(BACKGROUND_COLOR3_KEY)
        }
    }

    private fun swapFragments() {
        if (isSwapped) {
            parentFragmentManager.beginTransaction()
                .replace(R.id.fragment_container2, ThirdFragment.newInstance(fragment3BackgroundColor))
                .replace(R.id.fragment_container3, SecondFragment.newInstance(fragment2BackgroundColor))
                .commit()
        } else {
            parentFragmentManager.beginTransaction()
                .replace(R.id.fragment_container2, SecondFragment.newInstance(fragment2BackgroundColor))
                .replace(R.id.fragment_container3, ThirdFragment.newInstance(fragment3BackgroundColor))
                .commit()
        }
    }

    private fun updateFragmentBackgroundColors() {
        parentFragmentManager.findFragmentById(R.id.fragment_container2)?.let { fragment ->
            if (fragment is SecondFragment) {
                fragment.setBackgroundColor(fragment2BackgroundColor)
            } else if (fragment is ThirdFragment) {
                fragment.setBackgroundColor(fragment3BackgroundColor)
            }
        }

        parentFragmentManager.findFragmentById(R.id.fragment_container3)?.let { fragment ->
            if (fragment is SecondFragment) {
                fragment.setBackgroundColor(fragment2BackgroundColor)
            } else if (fragment is ThirdFragment) {
                fragment.setBackgroundColor(fragment3BackgroundColor)
            }
        }
    }

    companion object{
        private const val IS_SWAPPED_KEY = "firstFragment.isSwapped"
        private const val BACKGROUND_COLOR2_KEY = "firstFragment.fragment2BackgroundColor"
        private const val BACKGROUND_COLOR3_KEY = "firstFragment.fragment3BackgroundColor"
    }
}
