package com.example.fragments

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if(savedInstanceState == null){
            supportFragmentManager.beginTransaction()
                .add(R.id.fragment_container1, FirstFragment())
                .add(R.id.fragment_container2, SecondFragment())
                .add(R.id.fragment_container3, ThirdFragment())
                .commit()
        }
    }
}
