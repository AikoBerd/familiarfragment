package com.example.fragments

import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

class ThirdFragment : Fragment() {

    private var backgroundColor: Int = Color.WHITE

    companion object {
        private const val BACKGROUND_COLOR = "backgroundColor"

        fun newInstance(backgroundColor: Int): ThirdFragment {
            val fragment = ThirdFragment()
            val args = Bundle()
            args.putInt(BACKGROUND_COLOR, backgroundColor)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_third, container, false)
        arguments?.getInt(BACKGROUND_COLOR)?.let { color ->
            view.setBackgroundColor(color)
        }
        return view
    }

    fun setBackgroundColor(color: Int) {
        backgroundColor = color
        view?.setBackgroundColor(color)
    }
}
